###############################################################################
# Purpose: To load OS_WLR_ORDER_EVENT table in DRA schema.
# Used tables: os_wlr_order_evnt_ext_tbl
# Target table: WLR_ORDER_EVENT
#
# Change History:
#
# Date            Author                          Comments
# 13-NOV-2019     ANINDYA MAHANTY                 Created script for first map.
###############################################################################

#importing libraries
import sys
from pyspark.context import SparkContext
from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from datetime import datetime
import time
from pyspark.sql.functions import udf
from pyspark.sql.types import *

#initiating a spark session
spark = SparkSession\
        .builder\
        .appName("loading external table to DRA")\
        .getOrCreate()

#loading the dataframe from source
df_ext_tbl = spark\
                .read\
                .csv('C:\\Users\\609775743\\Desktop\\TAF4\\JAR_AWS\\work_stream2\\dev\\ext_table\\WLR_ORDER_EVENT\\os_wlr_order_evnt_ext_tbl.csv',\
                        inferSchema = False, header = True)

#de-duplication on the dataframe
df_distinct = df_ext_tbl.distinct()

#Renaming src_actvty_date
df_temp = df_distinct.withColumn('SRC_ACTVTY_DATE', F.col('RECORD_DATE'))
df_temp = df_temp.drop('RECORD_DATE')

#timestamp format
format = 'dd-MMM-yyyy HH:mm:ss'
#changing from string to timestamp for date columns
df_temp2 = df_temp.withColumn("KCI_DATE",\
            F.from_unixtime(F.unix_timestamp(F.col('KCI_DATE'), format))\
            .cast(TimestampType())) \
            .withColumn("SRC_ACTVTY_DATE", \
            F.from_unixtime(F.unix_timestamp(F.col('SRC_ACTVTY_DATE'), format))\
            .cast(TimestampType())) \

#populating data_date, load_date_time
df_to_insert = df_temp2.withColumn('DATA_DATE', F.date_add(F.current_timestamp(),-1)\
                    .cast(TimestampType()))\
                    .withColumn('LOAD_DATE_TIME' ,  F.current_timestamp())

#write file to dra loaction
df_to_insert.coalesce(1)\
        .write\
        .format("csv")\
        .mode("append")\
        .option("timestampFormat", "yyyy-MM-dd HH:mm:ss")\
        .option("header",  True)\
        .save("C:\\Users\\609775743\\Desktop\\TAF4\\JAR_AWS\\work_stream2\\dev\\dra\\OS_WLR_ORDER_EVNT_TEST")
