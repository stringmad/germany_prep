###############################################################################
# Purpose: To load WLR_ORDER_EVENT table in ODW schema.
# Used tables: D_WLR_ORDER_EVENT
# Target table: WLR_ORDER_EVENT
#
# Change History:
#
# Date            Author                          Comments
# 13-NOV-2019     ANINDYA MAHANTY                 Created script for first map.
###############################################################################
import sys
from pyspark.context import SparkContext
from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from datetime import datetime
import time
from pyspark.sql.functions import udf
from pyspark.sql.types import *

spark = SparkSession\
        .builder\
        .appName("loading DSA_OWNER table to ODW")\
        .getOrCreate()

#loading the dataframe from dsa ownersource
df_dsa_owner = spark.read.csv('dsa\D_WLR_ORDER_EVNT_TEST\*.csv', inferSchema = False,\
                header = True)

#loading target dataframe to load only delta
df_odw= spark.read.csv('odw\WLR_ORDER_EVENT_TEST\*.csv', inferSchema = False, \
                        header = True)

#check the count after doing a distinct on the dataframe
df_dsa_owner_distinct = df_dsa_owner.distinct()

#Change date columns to date from string
df_dsa_final = df_dsa_owner.withColumn("KCI_DATE", F.to_timestamp(F.col('KCI_DATE'))) \
            .withColumn("SRC_ACTVTY_DATE", F.to_timestamp(F.col('SRC_ACTVTY_DATE')))\
            .withColumn("DATA_DATE", F.to_timestamp(F.col('DATA_DATE')))\
            .withColumn("CREATE_DATE",F.to_timestamp(F.col('CREATE_DATE')))

df_odw = df_odw.withColumn("KCI_DATE", F.to_timestamp(F.col('KCI_DATE'))) \
            .withColumn("SRC_ACTVTY_DATE", F.to_timestamp(F.col('SRC_ACTVTY_DATE')))\
            .withColumn("DATA_DATE", F.to_timestamp(F.col('DATA_DATE')))\
            .withColumn("CREATE_DATE",F.to_timestamp(F.col('CREATE_DATE')))

#getting the delta from dsa
odw_max_create_date = df_odw.select("CREATE_DATE").agg(F.max(df_odw["CREATE_DATE"]))
odw_max_create_date = odw_max_create_date.collect()[0][0]

#filtering for delta data
df_delta_dsa = df_dsa_final.where(F.col('CREATE_DATE') > odw_max_create_date)

df_append = df_delta_dsa.select(['ORDER_NUM', 'ITEM_LINE', 'KCI_CODE', \
                                    'KCI_MSG', 'KCI_DATE', 'DELAY_RSN_CODE',\
                                'CLOCK_PAUSE','SRC_ACTVTY_DATE','DATA_DATE',\
                                'EVENT_SEQ_NUM','CREATE_BY','CREATE_DATE',\
                                'WATERMARK'])

#saving the file in dsa location
df_append.coalesce(1)\
        .write\
        .format("csv")\
        .mode("append")\
        .option("timestampFormat", "yyyy-MM-dd HH:mm:ss")\
        .option("header",  True)\
        .save("odw\WLR_ORDER_EVENT_TEST")
