###############################################################################
# Purpose: To load WLR_ORDER_EVENT table in DSA OWN schema.
# Used tables: OS_WLR_ORDER_EVENT
# Target table: WLR_ORDER_EVENT
#
# Change History:
#
# Date            Author                          Comments
# 13-NOV-2019     ANINDYA MAHANTY                 Initial Draft.
###############################################################################

#importing libraries
import sys
from pyspark.context import SparkContext
from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from datetime import datetime
import time
from pyspark.sql.functions import udf
from pyspark.sql.types import *
from pyspark.sql import Window as W

#creating a spark session
spark = SparkSession\
            .builder \
            .appName("Incremental_Insert_DSA")\
            .getOrCreate()

#loading the dataframe from dra
loading the dataframe from dra
df_dra = spark.read.csv('dra/OS_WLR_ORDER_EVENT_TEST/*.csv', inferSchema = True, header = True)

#loading the dataframe from dsa, this can be considered history data.
df_dsa = spark.read.csv('dsa/D_WLR_ORDER_EVNT_TEST/*.csv', inferSchema = True, header = True)

#changing to timestamp format in source DRA
df_dra = df_dra.withColumn("KCI_DATE", F.to_timestamp(F.col("KCI_DATE"))) \
            .withColumn("SRC_ACTVTY_DATE", F.to_timestamp(F.col("SRC_ACTVTY_DATE")))\
            .withColumn("DATA_DATE", F.to_timestamp(F.col("DATA_DATE")))\
            .withColumn("LOAD_DATE_TIME", F.to_timestamp(F.col("LOAD_DATE_TIME")))

#changing date format in source DSA
df_dsa = df_dsa.withColumn("KCI_DATE", F.to_timestamp(F.col("KCI_DATE"))) \
            .withColumn("SRC_ACTVTY_DATE", F.to_timestamp(F.col("SRC_ACTVTY_DATE")))\
            .withColumn("DATA_DATE", F.to_timestamp(F.col("DATA_DATE")))\
            .withColumn("CREATE_DATE", F.to_timestamp(F.col("CREATE_DATE")))

#getting the last load date from dsa to calculate the delta.
#this can be changed once we have a control table in place.
dsa_max_create_date = df_dsa.select("CREATE_DATE")\
                        .agg(F.max(df_dsa["CREATE_DATE"]))
dsa_max_create_date = dsa_max_create_date.collect()[0][0]

#truncating the timestamp in load_date_time in DRA.
df_dra = df_dra.withColumn("LOAD_DATE_TIME" , \
            F.date_trunc("day", "LOAD_DATE_TIME"))

#calculating today's data
df_dra_delta = df_dra.where(F.date_trunc('day' ,\
            F.col('LOAD_DATE_TIME') ) > dsa_max_create_date )

#specify window to create event_seq_num column to populate in the DSA table.
window_spec = W.partitionBy(['ORDER_NUM','ITEM_LINE']).\
                orderBy(['SRC_ACTVTY_DATE','KCI_DATE', 'KCI_CODE'])

#in the next steps, we will get the MAX_EVENT_SEQ_NUM from target table for
#repeating orders.
df_distinct_orders = df_dra_delta.drop_duplicates(['ORDER_NUM', 'ITEM_LINE'])
#getting the current event seq num
df_joined = df_dsa.alias('a').join(df_distinct_orders.alias('b'), \
            on =(df_dsa['ORDER_NUM'] == df_distinct_orders['ORDER_NUM']) & \
            (df_dsa['ITEM_LINE'] == df_distinct_orders['ITEM_LINE']))\
            .select('a.ORDER_NUM','a.ITEM_LINE','a.EVENT_SEQ_NUM')

df_joined = df_joined.withColumn('EVENT_SEQ_NUM', df_joined['EVENT_SEQ_NUM'].\
            cast(IntegerType()))

#getting the maximum event sequence number for the order_num, item_line
df_joined = df_joined.select(['ORDER_NUM', 'ITEM_LINE', 'EVENT_SEQ_NUM']) \
            .groupBy(['ORDER_NUM', 'ITEM_LINE'])\
            .max('EVENT_SEQ_NUM')\
            .withColumnRenamed('max(EVENT_SEQ_NUM)', 'MAX_EVENT_SEQ_NUM')

#creating dataframe for insert
df_merged = df_dra_delta.alias('dra').join(df_joined.alias('delta_updates'),\
            on =((df_dra_delta.ORDER_NUM == df_joined.ORDER_NUM) & \
            (df_dra_delta.ITEM_LINE == df_joined.ITEM_LINE)), how = 'left')\
            .select('dra.ORDER_NUM','dra.ITEM_LINE','dra.KCI_CODE','dra.KCI_MSG'\
            , 'dra.KCI_DATE', 'dra.DELAY_RSN_CODE', 'dra.CLOCK_PAUSE', \
            'dra.SRC_ACTVTY_DATE', 'dra.DATA_DATE',\
            'delta_updates.MAX_EVENT_SEQ_NUM')

#generating event seq number for the merged dataframe
#for repeating records it should be added to the MAX_EVENT_SEQ_NUM
df_to_insert = df_merged.withColumn('EVENT_SEQ_NUM', \
                F.coalesce(F.col('MAX_EVENT_SEQ_NUM'),\
                F.lit(0))+ F.row_number().over(window_spec))

#create_by and create_date column and dropping the MAX_EVENT_SEQ_NUM column.
df_insert = df_to_insert.drop('MAX_EVENT_SEQ_NUM')\
            .withColumn('CREATE_BY', F.lit('D30'))\
            .withColumn("CREATE_DATE", F.current_timestamp())

#saving the file in dsa location
df_insert.coalesce(1)\
        .write\
        .format("csv")\
        .mode("append")\
        .option("timestampFormat", "yyyy-MM-dd HH:mm:ss")\
        .option("header",  True)\
        .save("dsa\D_WLR_ORDER_EVNT_TEST")
