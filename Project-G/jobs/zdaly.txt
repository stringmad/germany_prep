Heavy SQL and data engineering skills: 

Familiarly with design and optimization of very large (> 1 TB) databases
Proper .mdf / .ldf file set up across different spindles
Clustered and non-clustered index creation and maintenance
Knowledge of query execution plans and optimization of such
Excellent stored procedure / TSQL programming ability including scalar- and table-valued functions, views, cursors, temporary tables, common table expressions, dynamic SQL, windowed functions
Heavy ETL experience: cleaning raw data and importing large (TB) files of data
Familiarly with transaction isolation levels
Familiarity with database backup / restore
 

Decent Python skills: 

Familiarly with standard Python data science libraries (statsmodels, pandas, numpy, scipy, sklearn, etc)
Experience with data wrangling in Python dataframes doing things such as filtering, sorting, joining, and analyzing data sets
Nice to have: experience with threading / asynchronous calls / functions, or experience with scaling large batch jobs
Some time series experience would be nice: understanding basic time series models and how to implement them
Detailed knowledge of any type of statistical model would be nice, for example regression, or clustering, mathematical understanding of how they actually work
 

Experience : 5-10 years

Great Culture & compensation.