def swap_case(s):
 t=str(s)
 newstring=str('')
 j=len(t)
 for i in range(0,j):
  if( t[i].isupper())==True:
   newstring+=(t[i].lower())
  elif (t[i].islower()) == True:
   newstring += (t[i].upper())
  else:
   newstring+=t[i]
 return newstring
 
if __name__ == '__main__':
    s = input()
    result = swap_case(s)
    print(result)