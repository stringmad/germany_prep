## How to use CONTINUE and BREAK statement within a loop in Python
def Kickstarter_Example_52():
    print()
    print(format('How to use CONTINUE and BREAK statement within a loop in Python',
                 '*^82'))
    import warnings
    warnings.filterwarnings("ignore")

    # Import the random module
    import random
    # Create a while loop # set "running" to true
    running = True
    # while running is true
    while running:
        # Create a random integer between 0 and 5
        s = random.randint(0,5)
        # If the integer is less than 3
        if s < 3:
            print(s, ': It is too small, starting again.')
            continue
        # If the integer is 4
        if s == 4:
            running = False
            print('It is 4! Changing running to false')
        # If the integer is 5,
        if s == 5:
            print('It is 5! Breaking Loop!')
            break
Kickstarter_Example_52()
*********How to use CONTINUE and BREAK statement within a loop in Python**********
0 : It is too small, starting again.
0 : It is too small, starting again.
It is 4! Changing running to false